
$.fn.andSelf = function() {
  return this.addBack.apply(this, arguments);
}


        $("#featured-slider-two").owlCarousel({
            items:4,
            nav:true,
            autoplay:true,
            dots:false,
			autoplayHoverPause:true,
			nav:true,
			navText: [
			  "<i class='fa fa-chevron-left'></i>",
			  "<i class='fa fa-chevron-right'></i>"
			],
            responsive: {
                0: {
                    items: 1,
                    slideBy:1
                },
                480: {
                    items: 2,
                    slideBy:1
                },
                991: {
                    items: 3,
                    slideBy:1
                },
                1000: {
                    items: 4,
                    slideBy:1
                },
            }            

        });
		

        // -------------------------------------------------------------
    // Accordion
    // -------------------------------------------------------------

        (function () {  
            $('.collapse').on('show.bs.collapse', function() {
                var id = $(this).attr('id');
                $('a[href="#' + id + '"]').closest('.card-header').addClass('active-card');
                $('a[href="#' + id + '"] .card-title span').html('<i class="fa fa-minus"></i>');
            });

            $('.collapse').on('hide.bs.collapse', function() {
                var id = $(this).attr('id');
                $('a[href="#' + id + '"]').closest('.card-header').removeClass('active-card');
                $('a[href="#' + id + '"] .card-title span').html('<i class="fa fa-plus"></i>');
            });
        }());


        // -------------------------------------------------------------
    //   Show Mobile Number
    // -------------------------------------------------------------  

    (function () {

        $('.show-number').on('click', function() {
            $('.hide-text').fadeIn(500, function() {
              $(this).addClass('hide');
            });  
            $('.hide-number').fadeIn(500, function() {
              $(this).addClass('show');
            });             
        });


    }());